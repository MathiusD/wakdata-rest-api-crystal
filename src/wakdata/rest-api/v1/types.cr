require "wakdata"

module Wakdata::RestAPI::V1::Type
  alias RAW_TYPES_ALLOWED = Wakdata::Action |
                            Wakdata::Blueprint |
                            Wakdata::CollectibleResource |
                            Wakdata::ItemType |
                            Wakdata::HarvestLoot |
                            Wakdata::ItemProperty |
                            Wakdata::Item |
                            Wakdata::JobItem |
                            Wakdata::RecipeCategory |
                            Wakdata::RecipeIngredient |
                            Wakdata::RecipeResult |
                            Wakdata::Recipe |
                            Wakdata::Resource |
                            Wakdata::ResourceType |
                            Wakdata::State

  alias HASH_TYPES_ALLOWED = Hash(String, Array(Wakdata::Action)) |
                             Hash(String, Array(Wakdata::Blueprint)) |
                             Hash(String, Array(Wakdata::CollectibleResource)) |
                             Hash(String, Array(Wakdata::ItemType)) |
                             Hash(String, Array(Wakdata::HarvestLoot)) |
                             Hash(String, Array(Wakdata::ItemProperty)) |
                             Hash(String, Array(Wakdata::Item)) |
                             Hash(String, Array(Wakdata::JobItem)) |
                             Hash(String, Array(Wakdata::RecipeCategory)) |
                             Hash(String, Array(Wakdata::RecipeIngredient)) |
                             Hash(String, Array(Wakdata::RecipeResult)) |
                             Hash(String, Array(Wakdata::Recipe)) |
                             Hash(String, Array(Wakdata::Resource)) |
                             Hash(String, Array(Wakdata::ResourceType)) |
                             Hash(String, Array(Wakdata::State))
end
