require "../commons"

require "../../cache"

require "kemal"
require "swagger"

module Wakdata::RestAPI::V1::Routes::Main
  macro define_main_route_for(all_data_name)
    get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/:version/{{all_data_name}}" do |env|
      version = env.params.url["version"]
      {{all_data_name}} = Wakdata::RestAPI::Cache.retrieve_{{all_data_name}}_for(version)
      if {{all_data_name}}
        Wakdata::RestAPI::V1::Format.return_value env, {{all_data_name}}, env.params.url["format"]
      else
        env.response.respond_with_status 404, "Unknown version : #{version}"
      end
    end

    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_{{all_data_name}}_ACTION << Swagger::Action.new(
      "get",
      "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/{version}/{{all_data_name}}",
      description: "Get all {{all_data_name}} with version specified",
      parameters: [
        Swagger::Parameter.new(
          "format",
          "path",
          description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
          default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
          required: true
        ),
        Swagger::Parameter.new("version", "path", default_value: "latest", required: true),
      ],
      responses: [
        Swagger::Response.new("200", "Success response"),
        Swagger::Response.new("400", "Wrong format provided"),
        Swagger::Response.new("404", "Unknown version"),
      ]
    )
  end
end
