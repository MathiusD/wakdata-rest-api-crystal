default_target: launch_docker_server

clean_config:
	@rm -f mongo-config.json

clean: clean_config
	@rm -rf docs
	@rm -rf bin
	@rm -rf lib
	@rm -f shard.lock

install:
	@shards install

build:
	@shards build
	
format:
	@crystal tool format --check

docs:
	@crystal docs lib/wakdata/src/wakdata.cr lib/wakdata-json/src/wakdata/json.cr lib/wakdata-cryomongo/src/wakdata/cryomongo.cr src/wakdata/rest-api.cr

launch_server: build clean_config
	@cp mongo-localhost-config.json mongo-config.json
	@./bin/wakdata-rest-api -p 5250

launch_mongo_local_server:
	@docker run --rm -p 27017:27017 mongo

docker_build: clean_config
	@cp mongo-docker-config.json mongo-config.json
	@docker-compose build

launch_docker_server: docker_build
	@docker-compose up