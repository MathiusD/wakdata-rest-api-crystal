require "bson"
require "http"
require "json"
require "yaml"

module Wakdata::RestAPI::V1::Format
  JSON_FORMAT     = "json"
  YAML_FORMAT     = "yaml"
  YML_FORMAT      = "yml"
  BSON_FORMAT     = "bson"
  FORMATS_ALLOWED = [
    JSON_FORMAT,
    YAML_FORMAT,
    YML_FORMAT,
    BSON_FORMAT,
  ]

  def self.return_value(env : HTTP::Server::Context, value : T, format : String?) forall T
    case format.downcase
    when JSON_FORMAT
      env.response.content_type = "application/json"
      return value.to_json
    when YAML_FORMAT, YML_FORMAT
      env.response.content_type = "application/yaml"
      return value.to_yaml
    when BSON_FORMAT
      env.response.content_type = "application/bson"
      return BSON.new(value).to_json
    else
      env.response.respond_with_status 400, "Unknown format : #{format}"
    end
  end
end
