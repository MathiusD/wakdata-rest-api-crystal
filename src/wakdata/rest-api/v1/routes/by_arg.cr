require "../commons"

require "../../cache"

require "kemal"
require "swagger"

module Wakdata::RestAPI::V1::Routes::By::Arg
  macro define_route_with_custom_arg_for_data(all_data_name, single_data_name, arg_name, arg_location, arg_type)
    get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/:version/{{all_data_name}}/{{arg_name}}/:{{arg_name}}" do |env|
      version = env.params.url["version"]
      begin
        {{arg_name}} = {{arg_type}}.new env.params.url["{{arg_name}}"]
      rescue
        env.response.respond_with_status 400,
        "Please give a valid {{arg_name}} (Matching with {{arg_type}} scheme)"
        next
      end
      {{all_data_name}} = Wakdata::RestAPI::Cache.retrieve_{{all_data_name}}_for(version)
      if {{all_data_name}}
        {{all_data_name}}_selected = {{all_data_name}}.select { |{{single_data_name}}|
          {{single_data_name}}.{{arg_location}} == {{arg_name}}
        }
        Wakdata::RestAPI::V1::Format.return_value env, {{all_data_name}}_selected, env.params.url["format"]
      else
        env.response.respond_with_status 404, "Unknown version : #{version}"
      end
    end

    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_{{all_data_name}}_ACTION << Swagger::Action.new(
      "get",
      "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/{version}/{{all_data_name}}/{{arg_name}}/{{{arg_name}}}",
      description: "Get {{all_data_name}} with version specified and match with {{arg_name}} provided",
      parameters: [
        Swagger::Parameter.new(
          "format",
          "path",
          description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
          default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
          required: true
        ),
        Swagger::Parameter.new("version", "path", default_value: "latest", required: true),
        Swagger::Parameter.new("{{arg_name}}", "path", required: true),
      ],
      responses: [
        Swagger::Response.new("200", "Success response"),
        Swagger::Response.new("400", "Wrong format provided"),
        Swagger::Response.new("404", "Invalid {{arg_name}} provided"),
        Swagger::Response.new("404", "Unknwon version"),
      ]
    )
  end

  macro define_route_with_custom_arg_and_custom_operation_for_data(all_data_name, single_data_name, arg_name, arg_type, custom_args, custom_operation)
    get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/:version/{{all_data_name}}/{{arg_name}}/:{{arg_name}}" do |env|
      version = env.params.url["version"]
      begin
        {{arg_name}} = {{arg_type}}.new env.params.url["{{arg_name}}"]
      rescue
        env.response.respond_with_status 400,
        "Please give a valid {{arg_name}} (Matching with {{arg_type}} scheme)"
        next
      end
      {% for custom_arg in custom_args %}
        {% if custom_arg["type"].nil? %}
          begin
            {{custom_arg["name"]}} = env.params.query["{{custom_arg["name"]}}"]
          rescue
            env.response.respond_with_status 400, "Please give a {{custom_arg["name"]}}"
            next
          end
        {% else %}
          begin
            {{custom_arg["name"]}} = {{custom_arg["type"]}}.new env.params.url["{{custom_arg["name"]}}"]
          rescue
            env.response.respond_with_status 400,
            "Please give a valid {{custom_arg["name"]}} (Matching with {{custom_arg["type"]}} scheme)"
            next
          end
        {% end %}
      {% end %}
      {{all_data_name}} = Wakdata::RestAPI::Cache.retrieve_{{all_data_name}}_for(version)
      if {{all_data_name}}
        {{all_data_name}}_selected = {{all_data_name}}.select { |{{single_data_name}}|
          {{custom_operation}}
        }
        Wakdata::RestAPI::V1::Format.return_value env, {{all_data_name}}_selected, env.params.url["format"]
      else
        env.response.respond_with_status 404, "Unknown version : #{version}"
      end
    end

    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_{{all_data_name}}_ACTION << Swagger::Action.new(
      "get",
      "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/{version}/{{all_data_name}}/{{arg_name}}/{{{arg_name}}}",
      description: "Get {{all_data_name}} with version specified and match with {{arg_name}} provided",
      parameters: [
        Swagger::Parameter.new(
          "format",
          "path",
          description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
          default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
          required: true
        ),
        Swagger::Parameter.new("version", "path", default_value: "latest", required: true),
        Swagger::Parameter.new("{{arg_name}}", "path", required: true),
        {% for custom_arg in custom_args %}
          Swagger::Parameter.new("{{custom_arg["name"]}}", "query", required: true),
        {% end %}
      ],
      responses: [
        Swagger::Response.new("200", "Success response"),
        Swagger::Response.new("400", "Wrong format provided"),
        Swagger::Response.new("404", "Invalid {{arg_name}} provided"),
        Swagger::Response.new("404", "Unknwon version"),
      ]
    )
  end
end
