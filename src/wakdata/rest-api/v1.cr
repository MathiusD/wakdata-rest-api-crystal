require "./v1/commons"
require "./v1/routes"
require "./v1/swagger"

require "kemal"
require "semantic_version"
require "swagger/http/handler"

module Wakdata::RestAPI::V1
  SWAGGER_API_HANDLER = Swagger::HTTP::APIHandler.new(
    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_BUILDER.built,
    "#{BASE_PATH}/",
    # Work around for add base path but preserve generated path by APIHandler
    "#{BASE_PATH}/openapi/v#{
  SemanticVersion.parse(
      Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_BUILDER.built.openapi_version
    ).major
}/swagger.json"
  )
  SWAGGER_WEB_HANDLER = Swagger::HTTP::WebHandler.new(
    "#{BASE_PATH}/swagger",
    SWAGGER_API_HANDLER.api_url
  )
end

add_handler Wakdata::RestAPI::V1::SWAGGER_API_HANDLER
add_handler Wakdata::RestAPI::V1::SWAGGER_WEB_HANDLER
