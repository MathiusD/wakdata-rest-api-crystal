require "../data"

require "./routes/by_arg"
require "./routes/by_id"
require "./routes/by_trad"
require "./routes/main"
require "./routes/version"
require "./swagger"

require "wakdata"

module Wakdata::RestAPI::V1::Routes
  macro define_route_for_data_without_id(all_data_name, single_data_name, constant_name, struct_name)
    Wakdata::RestAPI::V1::Routes::Version.define_version_route_for {{all_data_name}}, {{constant_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.define_swagger_actions_constant_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::Main.define_main_route_for {{all_data_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.add_swagger_controller_for {{all_data_name}},
      "{{all_data_name}}",
      "Endpoints for retrieve {{all_data_name}}"
  end

  macro define_route_for_data_with_id_in_data(all_data_name, single_data_name, constant_name, struct_name)
    Wakdata::RestAPI::V1::Routes::Version.define_version_route_for {{all_data_name}}, {{constant_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.define_swagger_actions_constant_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::Main.define_main_route_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::By::Id.define_route_with_id_for_data_with_id_in_data {{all_data_name}}, {{single_data_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.add_swagger_controller_for {{all_data_name}},
      "{{all_data_name}}",
      "Endpoints for retrieve {{all_data_name}}"
  end

  macro define_route_for_data_with_id_in_def(all_data_name, single_data_name, constant_name, struct_name)
    Wakdata::RestAPI::V1::Routes::Version.define_version_route_for {{all_data_name}}, {{constant_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.define_swagger_actions_constant_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::Main.define_main_route_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::By::Id.define_route_with_id_for_data_with_id_in_def {{all_data_name}}, {{single_data_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.add_swagger_controller_for {{all_data_name}},
      "{{all_data_name}}",
      "Endpoints for retrieve {{all_data_name}}"
  end

  macro define_route_for_data_in_specific_location_with_id_in_def(all_data_name, single_data_name, constant_name, struct_name)
    Wakdata::RestAPI::V1::Routes::Version.define_version_route_for {{all_data_name}}, {{constant_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.define_swagger_actions_constant_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::Main.define_main_route_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::By::Id.define_route_with_id_for_data_with_id_in_def {{all_data_name}}, {{single_data_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.add_swagger_controller_for {{all_data_name}},
      "{{all_data_name}}",
      "Endpoints for retrieve {{all_data_name}}"
  end

  macro define_route_for_data_with_id_in_specific_location(all_data_name, single_data_name, constant_name, struct_name, id_location)
    Wakdata::RestAPI::V1::Routes::Version.define_version_route_for {{all_data_name}}, {{constant_name}}
    Wakdata::RestAPI::V1::CurrentSwagger.define_swagger_actions_constant_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::Main.define_main_route_for {{all_data_name}}
    Wakdata::RestAPI::V1::Routes::By::Id.define_route_with_id_for_data {{all_data_name}}, {{single_data_name}}, {{id_location}}
    Wakdata::RestAPI::V1::CurrentSwagger.add_swagger_controller_for {{all_data_name}},
      "{{all_data_name}}",
      "Endpoints for retrieve {{all_data_name}}"
  end
end

Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def actions,
  action,
  ACTIONS,
  Wakdata::Action
Wakdata::RestAPI::V1::Routes.define_route_for_data_without_id blueprints,
  blueprint,
  BLUEPRINTS,
  Wakdata::Blueprint
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data blueprints,
  blueprint,
  blueprintId,
  blueprint_id,
  UInt32
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_data collectibleResources,
  collectibleResource,
  COLLECTIBLE_RESOURCES,
  Wakdata::CollectibleResource
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data collectibleResources,
  collectibleResource,
  resourceId,
  resource_id,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data collectibleResources,
  collectibleResource,
  skillId,
  skill_id,
  UInt32
Wakdata::RestAPI::V1::Routes.define_route_for_data_in_specific_location_with_id_in_def equipmentItemTypes,
  equipmentItemType,
  EQUIPMENT_ITEM_TYPES,
  Wakdata::ItemType
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_data harvestLoots,
  harvestLoot,
  HARVEST_LOOTS,
  Wakdata::HarvestLoot
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_data itemProperties,
  itemProperty,
  ITEM_PROPERTIES,
  Wakdata::ItemProperty
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def itemTypes,
  itemType,
  ITEM_TYPES,
  Wakdata::ItemType
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data itemTypes,
  itemType,
  title,
  title
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_specific_location items,
  item,
  ITEMS,
  Wakdata::Item,
  definition.item.id
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data items,
  item,
  itemTypeId,
  definition.item.base_parameters.item_type_id,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data items,
  item,
  rarity,
  definition.item.base_parameters.rarity,
  UInt8
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data items,
  item,
  level,
  definition.item.level,
  UInt16
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data items,
  item,
  title,
  title
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data items,
  item,
  description,
  description
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def jobItems,
  jobItem,
  JOB_ITEMS,
  Wakdata::JobItem
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data jobItems,
  jobItem,
  itemTypeId,
  definition.item_type_id,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data jobItems,
  jobItem,
  rarity,
  definition.rarity,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data jobItems,
  jobItem,
  level,
  definition.level,
  UInt16
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data jobItems,
  jobItem,
  title,
  title
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data jobItems,
  jobItem,
  description,
  description
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def recipeCategories,
  recipeCategory,
  RECIPE_CATEGORIES,
  Wakdata::RecipeCategory
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data recipeCategories,
  recipeCategory,
  title,
  title
Wakdata::RestAPI::V1::Routes.define_route_for_data_without_id recipeIngredients,
  recipeIngredient,
  RECIPE_INGREDIENTS,
  Wakdata::RecipeIngredient
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data recipeIngredients,
  recipeIngredient,
  recipeId,
  recipe_id,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data recipeIngredients,
  recipeIngredient,
  itemId,
  item_id,
  UInt32
Wakdata::RestAPI::V1::Routes.define_route_for_data_without_id recipeResults,
  recipeResult,
  RECIPE_RESULTS,
  Wakdata::RecipeResult
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data recipeResults,
  recipeResult,
  recipeId,
  recipe_id,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data recipeResults,
  recipeResult,
  productedItemId,
  producted_item_id,
  UInt32
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_data recipes,
  recipe,
  RECIPES,
  Wakdata::Recipe
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data recipes,
  recipe,
  categoryId,
  category_id,
  UInt32
Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_for_data recipes,
  recipe,
  level,
  level,
  UInt16
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def resources,
  resource,
  RESOURCES,
  Wakdata::Resource
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data resources,
  resource,
  title,
  title
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def resourceTypes,
  resourceType,
  RESOURCE_TYPES,
  Wakdata::ResourceType
Wakdata::RestAPI::V1::Routes::By::Trad.define_route_with_trad_regex_for_data resourceTypes,
  resourceType,
  title,
  title
Wakdata::RestAPI::V1::Routes.define_route_for_data_with_id_in_def states,
  state,
  STATES,
  Wakdata::State
