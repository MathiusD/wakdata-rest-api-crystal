require "./rest-api/commons"

require "./rest-api/cache"
require "./rest-api/data"
require "./rest-api/headers"
require "./rest-api/v1"

require "kemal"

Kemal.run do |config|
  server = config.server.not_nil!
  server.bind_tcp config.host_binding, config.port, reuse_port: true
end
