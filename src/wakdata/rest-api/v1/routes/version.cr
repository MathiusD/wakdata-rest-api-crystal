require "../../cache"
require "../../cryomongo"

require "../format"
require "../swagger"
require "../types"

require "kemal"
require "json"
require "swagger"
require "wakdata"
require "wakdata-json/src/wakdata/json"

module Wakdata::RestAPI::V1::Routes::Version
end

Wakdata::Utils::Macros.generate_struct Wakdata::RestAPI::V1::Routes::Version::Info, {
  is_module:       false,
  is_required:     true,
  attribute_name:  cache,
  attribute_class: Array(String),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  db,
  attribute_class: Array(String),
}

Wakdata::RestAPI::V1::CurrentSwagger.define_swagger_actions_constant_for VERSIONS

module Wakdata::RestAPI::V1::Routes::Version
  TYPES      = Array(String).new
  CACHE_HASH = Hash(String, Wakdata::RestAPI::V1::Type::HASH_TYPES_ALLOWED).new

  def self.versions_of(name : String) : Info
    mongo_data = Wakdata::RestAPI::CryoMongo.retrieve_versions(name)
    if mongo_data == nil
      mongo_versions = Array(String).new
    else
      mongo_versions = mongo_data.not_nil!.versions
    end
    cache_data = CACHE_HASH[name]
    if cache_data == nil
      cache_data_keys = Array(String).new
    else
      cache_data_keys = cache_data.keys
    end
    return Info.new(
      cache_data_keys,
      mongo_versions
    )
  end

  macro define_version_route_for(all_data_name, constant_name)
    def Wakdata::RestAPI::V1::Routes::Version.versions_of_{{all_data_name}}() : Wakdata::RestAPI::V1::Routes::Version::Info
      mongo_data = Wakdata::RestAPI::CryoMongo.retrieve_versions("{{all_data_name}}")
      if mongo_data == nil
        mongo_versions = Array(String).new
      else
        mongo_versions = mongo_data.not_nil!.versions
      end
      return Wakdata::RestAPI::V1::Routes::Version::Info.new(
        Wakdata::RestAPI::Cache::{{constant_name}}_DATA.keys,
        mongo_versions
      )
    end
    
    get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/versions/{{all_data_name}}" do |env|
      Wakdata::RestAPI::V1::Format.return_value env,
        Wakdata::RestAPI::V1::Routes::Version.versions_of_{{all_data_name}},
        env.params.url["format"]
    end

    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_VERSIONS_ACTION << Swagger::Action.new(
      "get",
      "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/versions/{{all_data_name}}",
      description: "Get all versions already retrieved for {{all_data_name}}",
      parameters: [
        Swagger::Parameter.new(
          "format",
          "path",
          description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
          default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
          required: true
        ),
      ],
      responses: [
        Swagger::Response.new("200", "Success response"),
        Swagger::Response.new("400", "Wrong format provided"),
      ]
    )

    Wakdata::RestAPI::V1::Routes::Version::TYPES << "{{all_data_name}}"
    Wakdata::RestAPI::V1::Routes::Version::CACHE_HASH["{{all_data_name}}"] = Wakdata::RestAPI::Cache::{{constant_name}}_DATA
  end
end

get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/versions/latest" do |env|
  Wakdata::RestAPI::V1::Format.return_value env,
    {"version" => Wakdata::RestAPI::Cache.latest_version},
    env.params.url["format"]
end

Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_VERSIONS_ACTION << Swagger::Action.new(
  "get",
  "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/versions/latest",
  description: "Show latest version can be retrieved",
  parameters: [
    Swagger::Parameter.new(
      "format",
      "path",
      description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
      default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
      required: true
    ),
  ],
  responses: [
    Swagger::Response.new("200", "Success response"),
    Swagger::Response.new("400", "Wrong format provided"),
  ]
)

get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/versions/all" do |env|
  versions = Hash(String, Wakdata::RestAPI::V1::Routes::Version::Info).new
  Wakdata::RestAPI::V1::Routes::Version::TYPES.each { |version_name|
    versions[version_name] = Wakdata::RestAPI::V1::Routes::Version.versions_of version_name
  }
  Wakdata::RestAPI::V1::Format.return_value env, versions, env.params.url["format"]
end

Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_VERSIONS_ACTION << Swagger::Action.new(
  "get",
  "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/versions/all",
  description: "Get all versions already retrieved for all data types",
  parameters: [
    Swagger::Parameter.new(
      "format",
      "path",
      description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
      default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
      required: true
    ),
  ],
  responses: [
    Swagger::Response.new("200", "Success response"),
    Swagger::Response.new("400", "Wrong format provided"),
  ]
)

Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_BUILDER.add(
  Swagger::Controller.new("Version", "Versions Info", Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_VERSIONS_ACTION)
)
