require "../commons"

require "../../cache"

require "kemal"
require "swagger"

module Wakdata::RestAPI::V1::Routes::By::Id
  macro define_route_with_id_for_data(all_data_name, single_data_name, id_location)
    get "#{Wakdata::RestAPI::V1::BASE_PATH}/:format/:version/{{single_data_name}}/:id" do |env|
      version = env.params.url["version"]
      begin
        id = UInt32.new env.params.url["id"]
      rescue
        env.response.respond_with_status 400, "Please give a valid id (Matching with UInt32 scheme)"
        next
      end
      {{all_data_name}} = Wakdata::RestAPI::Cache.retrieve_{{all_data_name}}_for(version)
      if {{all_data_name}}
        {{single_data_name}} = {{all_data_name}}.find { |{{single_data_name}}|
          {{single_data_name}}.{{id_location}} == id
        }
        if {{single_data_name}}
          Wakdata::RestAPI::V1::Format.return_value env, {{single_data_name}}, env.params.url["format"]
        else
          env.response.respond_with_status 404, "{{single_data_name}} not found with following id : #{id}"
        end
      else
        env.response.respond_with_status 404, "Unknown version : #{version}"
      end
    end

    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_{{all_data_name}}_ACTION << Swagger::Action.new(
      "get",
      "#{Wakdata::RestAPI::V1::BASE_PATH}/{format}/{version}/{{single_data_name}}/{id}",
      description: "Get specific {{single_data_name}} with version specified and id provided",
      parameters: [
        Swagger::Parameter.new(
          "format",
          "path",
          description: "Format of data (Formats allowed : #{Wakdata::RestAPI::V1::Format::FORMATS_ALLOWED})",
          default_value: Wakdata::RestAPI::V1::Format::JSON_FORMAT,
          required: true
        ),
        Swagger::Parameter.new("version", "path", default_value: "latest", required: true),
        Swagger::Parameter.new("id", "path", required: true),
      ],
      responses: [
        Swagger::Response.new("200", "Success response"),
        Swagger::Response.new("400", "Wrong format provided"),
        Swagger::Response.new("404", "Invalid id provided"),
        Swagger::Response.new("404", "Unknown version or data with id provided not found"),
      ]
    )
  end

  macro define_route_with_id_for_data_with_id_in_data(all_data_name, single_data_name)
    Wakdata::RestAPI::V1::Routes::By::Id.define_route_with_id_for_data {{all_data_name}},
      {{single_data_name}},
      id
  end

  macro define_route_with_id_for_data_with_id_in_def(all_data_name, single_data_name)
    Wakdata::RestAPI::V1::Routes::By::Id.define_route_with_id_for_data {{all_data_name}},
      {{single_data_name}},
      definition.id
  end
end
