require "bson"
require "cryomongo"
require "json"

module Wakdata::RestAPI::CryoMongo
  struct Config
    include JSON::Serializable

    @[JSON::Field(key: "mongo_connection_string")]
    getter mongo_connection_string : String

    @[JSON::Field(key: "database")]
    getter database : String

    def initialize(@mongo_connection_string : String, @database : String)
    end
  end

  CONFIG_FILE   = File.open("./mongo-config.json")
  CONFIG        = Config.from_json CONFIG_FILE
  CLIENT        = Mongo::Client.new CONFIG.mongo_connection_string
  DB            = CLIENT[CONFIG.database]
  VERSIONS_DATA = DB["versions"]

  record VersionData,
    data_name : String,
    versions : Array(String) = Array(String).new,
    _id : BSON::ObjectId = BSON::ObjectId.new,
    creation_date : Time = Time.utc do
    include BSON::Serializable
    include JSON::Serializable
  end

  def self.retrieve_versions(all_data_name : String) : VersionData?
    raw_data = VERSIONS_DATA.find_one({data_name: all_data_name})
    if raw_data != nil
      VersionData.from_bson raw_data.not_nil!
    else
      return nil
    end
  end

  def self.add_version_to_mongo_versions(all_data_name : String, version : String)
    old_version_data = retrieve_versions(all_data_name)
    if old_version_data == nil
      new_version_data = VersionData.new data_name: all_data_name
    else
      new_version_data = old_version_data.not_nil!
    end
    if !(new_version_data.versions.includes?(version))
      new_version_data.versions << version
      if old_version_data != nil
        VERSIONS_DATA.replace_one old_version_data.not_nil!, new_version_data
      else
        VERSIONS_DATA.insert_one(new_version_data)
      end
    end
  end

  macro define_retrieve_data_for(all_data_name, struct_name)
    def Wakdata::RestAPI::CryoMongo.retrieve_{{all_data_name}}_for(version : String) : Array({{struct_name}})?
      begin
        return {{struct_name}}.get_from_mongo DB, "{{all_data_name}}_#{version}"
      rescue exc
        Log.error exception: exc,
          &.emit(
            "Error occurred during retrieve of data from mongo",
            data_type: "{{struct_name}}",
            version: version
          )
        return nil
      end
    end
  end

  macro define_save_data_for(all_data_name, struct_name)
    def Wakdata::RestAPI::CryoMongo.save_{{all_data_name}}_for(version : String, data : Array({{struct_name}}))
      begin
        {{struct_name}}.put_in_mongo(
          Wakdata::RestAPI::CryoMongo::DB,
          "{{all_data_name}}_#{version}",
          data
        )
        Wakdata::RestAPI::CryoMongo.add_version_to_mongo_versions("{{all_data_name}}", version)
      rescue exc
        Log.error exception: exc,
          &.emit(
            "Error occurred during save of data in mongo",
            data_type: "{{struct_name}}",
            version: version
          )
      end
    end
  end
end
