require "../commons"

module Wakdata::RestAPI::V1
  VERSION   = Wakdata::RestAPI::VERSION
  BASE_PATH = "#{Wakdata::RestAPI::API_PATH}/v1"
end
