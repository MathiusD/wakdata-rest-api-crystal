require "./cache"

require "wakdata"
require "wakdata-json/src/wakdata/json"
require "wakdata-cryomongo/src/wakdata/cryomongo"

Wakdata::RestAPI::Cache.define_retrieve_data_for actions,
  ACTIONS,
  Wakdata::Action
Wakdata::RestAPI::Cache.define_retrieve_data_for blueprints,
  BLUEPRINTS,
  Wakdata::Blueprint
Wakdata::RestAPI::Cache.define_retrieve_data_for collectibleResources,
  COLLECTIBLE_RESOURCES,
  Wakdata::CollectibleResource
Wakdata::RestAPI::Cache.define_retrieve_data_with_specific_option_for equipmentItemTypes,
  EQUIPMENT_ITEM_TYPES,
  Wakdata::ItemType,
  {version: version, name: "equipmentItemTypes"}
Wakdata::RestAPI::Cache.define_retrieve_data_for harvestLoots,
  HARVEST_LOOTS,
  Wakdata::HarvestLoot
Wakdata::RestAPI::Cache.define_retrieve_data_for itemProperties,
  ITEM_PROPERTIES,
  Wakdata::ItemProperty
Wakdata::RestAPI::Cache.define_retrieve_data_for itemTypes,
  ITEM_TYPES,
  Wakdata::ItemType
Wakdata::RestAPI::Cache.define_retrieve_data_for items,
  ITEMS,
  Wakdata::Item
Wakdata::RestAPI::Cache.define_retrieve_data_for jobItems,
  JOB_ITEMS,
  Wakdata::JobItem
Wakdata::RestAPI::Cache.define_retrieve_data_for recipeCategories,
  RECIPE_CATEGORIES,
  Wakdata::RecipeCategory
Wakdata::RestAPI::Cache.define_retrieve_data_for recipeIngredients,
  RECIPE_INGREDIENTS,
  Wakdata::RecipeIngredient
Wakdata::RestAPI::Cache.define_retrieve_data_for recipeResults,
  RECIPE_RESULTS,
  Wakdata::RecipeResult
Wakdata::RestAPI::Cache.define_retrieve_data_for recipes,
  RECIPES,
  Wakdata::Recipe
Wakdata::RestAPI::Cache.define_retrieve_data_for resources,
  RESOURCES,
  Wakdata::Resource
Wakdata::RestAPI::Cache.define_retrieve_data_for resourceTypes,
  RESOURCE_TYPES,
  Wakdata::ResourceType
Wakdata::RestAPI::Cache.define_retrieve_data_for states,
  STATES,
  Wakdata::State
