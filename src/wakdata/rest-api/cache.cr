require "./cryomongo"
require "./json"

require "kemal"

module Wakdata::RestAPI::Cache
  struct Version
    @latest_version : String
    @fetch_date : Time::Span
    @min_fetch_interval : Time::Span

    def initialize(@min_fetch_interval : Time::Span = 5.minute)
      @latest_version = Wakdata::Json.get_version
      @fetch_date = Time.monotonic
      Log.info &.emit("Latest version : #{@latest_version} retrieved from cdn")
    end

    def retrieve_latest_version
      @latest_version = Wakdata::Json.get_version
      @fetch_date = Time.monotonic
      Log.info &.emit("Latest version : #{@latest_version} retrieved from cdn")
    end

    def latest_version : String
      if (Time.monotonic - @fetch_date) >= @min_fetch_interval
        retrieve_latest_version
      end
      @latest_version
    end
  end

  VERSION = Version.new

  def self.latest_version : String
    VERSION.latest_version
  end

  macro define_retrieve_data_with_specific_option_for(all_data_name, constant_name, struct_name, specific_options)
    Wakdata::RestAPI::Cache::{{constant_name}}_DATA = Hash(String, Array({{struct_name}})).new

    def Wakdata::RestAPI::Cache.retrieve_{{all_data_name}}_for(version : String) : Array({{struct_name}})?
      if version == "latest"
        return Wakdata::RestAPI::Cache.retrieve_{{all_data_name}}_for(Wakdata::RestAPI::Cache.latest_version)
      else
        if !Wakdata::RestAPI::Cache::{{constant_name}}_DATA.has_key?(version)
          Log.info &.emit(
            "{{all_data_name}} with following version #{version} not found in memory cache"
          )
          mongo_data = Wakdata::RestAPI::CryoMongo.retrieve_{{all_data_name}}_for(version)
          if mongo_data == nil || mongo_data.not_nil!.size == 0
            Log.info &.emit(
              "{{all_data_name}} with following version #{version} not found in mongo"
            )
            Wakdata::RestAPI::Cache::{{constant_name}}_DATA[version] = Wakdata::RestAPI::Json.retrieve_{{all_data_name}}_for(
              version
            ).not_nil!
            Log.info &.emit(
              "{{all_data_name}} with following version #{version} has been retrieved from json cdn"
            )
            if mongo_data != nil
              Wakdata::RestAPI::CryoMongo.save_{{all_data_name}}_for(
                version, Wakdata::RestAPI::Cache::{{constant_name}}_DATA[version]
              )
              Log.info &.emit(
                "{{all_data_name}} with following version #{version} has been saved in mongo"
              )
            else
              Log.info &.emit(
                "{{all_data_name}} with following version #{version} isn't been saved in mongo because error occured during fetch in mongo (Cf previous logs)"
              )
            end
          else
            Wakdata::RestAPI::Cache::{{constant_name}}_DATA[version] = mongo_data.not_nil!
            Log.info &.emit(
              "{{all_data_name}} with following version #{version} has been retrieved from mongo"
            )
          end
        end
        return Wakdata::RestAPI::Cache::{{constant_name}}_DATA[version]
      end
    end

    Wakdata::RestAPI::Json.define_retrieve_data_with_specific_option_for {{all_data_name}},
      {{struct_name}},
      {{specific_options}}

    Wakdata::RestAPI::CryoMongo.define_retrieve_data_for {{all_data_name}},
      {{struct_name}}
    Wakdata::RestAPI::CryoMongo.define_save_data_for {{all_data_name}},
      {{struct_name}}
  end

  macro define_retrieve_data_for(all_data_name, constant_name, struct_name)
    Wakdata::RestAPI::Cache.define_retrieve_data_with_specific_option_for {{all_data_name}},
      {{constant_name}},
      {{struct_name}},
      {version: version}
  end
end
