# Wakdata REST API - Crystal

[![Pipeline Status](https://gitlab.com/MathiusD/wakdata-rest-api-crystal/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/wakdata-rest-api-crystal/-/pipelines)

Wakdata REST API is REST API written is crystal for query public data of MMORPG Wakfu from JSON files distributed in Wakfu CDN
