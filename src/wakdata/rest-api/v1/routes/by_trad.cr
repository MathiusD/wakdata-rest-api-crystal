require "./by_arg"

module Wakdata::RestAPI::V1::Routes::By::Trad
  macro define_route_with_trad_regex_for_data(all_data_name, single_data_name, tradLocation, tradName)
    Wakdata::RestAPI::V1::Routes::By::Arg.define_route_with_custom_arg_and_custom_operation_for_data {{all_data_name}},
      {{single_data_name}},
      {{tradName}}Regex,
      Regex,
      [
        {
          name: lang,
        },
      ],
      if {{single_data_name}}.{{tradLocation}} != nil
        {{tradName}} = {{single_data_name}}.{{tradLocation}}.not_nil!
        if {{tradName}}[lang] != nil
          {{tradName}}Regex.match({{tradName}}[lang].not_nil!) != nil
        else
          false
        end
      else
        false
      end
  end
end
