require "kemal"

static_headers do |response, filepath, filestat|
  response.headers.add("Access-Control-Allow-Origin", "*")
  response.headers.add("Content-Size", filestat.size.to_s)
  response.headers.add("X-Robots-Tag", "noindex")
end
