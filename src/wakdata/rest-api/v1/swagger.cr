require "./commons"

require "swagger"

module Wakdata::RestAPI::V1::CurrentSwagger
  SWAGGER_BUILDER = Swagger::Builder.new(
    title: "Wakdata REST API",
    version: Wakdata::RestAPI::V1::VERSION
  )

  macro define_swagger_actions_constant_for(all_data_name)
   Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_{{all_data_name}}_ACTION = Array(Swagger::Action).new
  end

  macro add_swagger_controller_for(all_data_name, controller_name, controller_desc)
    Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_BUILDER.add(
      Swagger::Controller.new(
        {{controller_name}},
        {{controller_desc}},
        Wakdata::RestAPI::V1::CurrentSwagger::SWAGGER_{{all_data_name}}_ACTION
      )
    )
  end
end
