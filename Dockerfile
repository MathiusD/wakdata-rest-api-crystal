FROM crystallang/crystal:1.6-alpine AS builder

WORKDIR /build

COPY ./shard.yml ./shard.yml
RUN shards install --without-development

COPY ./src ./src
RUN shards build --release --static

FROM alpine:latest

COPY --from=builder /build/bin/wakdata-rest-api /wakdata-rest-api
COPY ./mongo-config.json /mongo-config.json

ARG KEMAL_ENV=production

EXPOSE 8080

CMD [ "/wakdata-rest-api", "-p", "8080" ]
