module Wakdata::RestAPI::Json
  macro define_retrieve_data_with_specific_option_for(all_data_name, struct_name, specific_options)
		def Wakdata::RestAPI::Json.retrieve_{{all_data_name}}_for(version : String) : Array({{struct_name}})?
			begin
				return {{struct_name}}.get_jsons(
					{% for specific_option_name, specific_option_value in specific_options %}
						{{specific_option_name}}: {{specific_option_value}},
					{% end %}
				)
			rescue exc
				Log.error exception: exc,
					&.emit(
						"Error occurred during retrieve of data from cdn",
						data_type: "{{struct_name}}",
						version: version
					)
				return nil
			end
		end
	end
end
